from OCC.BRepPrimAPI import BRepPrimAPI_MakeBox as Box
from OCC.BRepPrimAPI import BRepPrimAPI_MakePrism as Extrude
from OCC.BRepAlgoAPI import BRepAlgoAPI_Fuse as Fuse
from OCC.gp import gp_Vec as Vect
#from OCC.gp import gp_Pnt as Pnt
from OCC.gp import gp_Circ as Circ
from OCC.gp import gp_Ax2 as Coord
from OCC.BRepBuilderAPI import BRepBuilderAPI_MakeEdge as Edge
from OCC.BRepBuilderAPI import BRepBuilderAPI_MakeWire as Wire
from OCC.BRepBuilderAPI import BRepBuilderAPI_MakeFace as Face

class Extrusion_Connector():
	width = 15
	extrusion_size = 15
	thickness = 6
	hole_radius = 2.5
	def __init__(self, width=15, extrusion_size=width, thickness=6,
			hole_radius=2.5):
		self.width = width
		self.extrusion_size = extrusion_size
		self.thickness = thickness
		self.hole_radius = hole_radius
#		self.make()

#	def make(self):
#		base_size = self.extrusion_size + self.thickness
#		base = Part.makeBox(self.width, base_size, base_size)
#		extr_chan = Part.makeBox(self.width, self.extrusion_size, self.extrusion_size,
#		                         Vector(0,self.thickness,0))
#		b_mount_hole = Part.makeCylinder(self.hole_radius, self.thickness,
#				Vector(0,0,0), Vector(0,1,0))
#		cutout = b_mount_hole#Part.Compound([extr_chan, b_mount_hole])
		x = Box(20,1,1)
		y = Box(1,20,1)
		z = Box(1,1,20)
		origin = Fuse(x.Shape(), Fuse(y.Shape(), z.Shape()).Shape())

		# Draws a circle geometry using gp_Ax22d and radius=4
		ci1 = Circ(Coord(), 4)
		circle = Face(Wire(Edge(ci1).Edge()).Wire()).Face()
		extrude_dir = Vect(0,10,20)
		cyl = Extrude(circle, extrude_dir)
		self.part = Fuse(origin.Shape(), cyl.Shape())
#		self.part = base#.cut(cutout)
#		self.part = cutout

	def Shape(self):
		return self.part.Shape()
