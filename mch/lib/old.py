import Part
from FreeCAD import Vector

class Extrusion_Bracket():
	width = 15
	extrusion_size = 15
	thickness = 6
	hole_radius = 2.5
	def __init__(self, width=15, extrusion_size=width, thickness=6,
			hole_radius=2.5):
		self.width = width
		self.extrusion_size = extrusion_size
		self.thickness = thickness
		self.hole_radius = hole_radius
		self.make()

	def make(self):
		base_size = self.extrusion_size + self.thickness
		base = Part.makeBox(self.width, base_size, base_size)
		extr_chan = Part.makeBox(self.width, self.extrusion_size, self.extrusion_size,
		                         Vector(0,self.thickness,0))
		b_mount_hole = Part.makeCylinder(self.hole_radius, self.thickness,
				Vector(0,0,0), Vector(0,1,0))
#		cutout = b_mount_hole#Part.Compound([extr_chan, b_mount_hole])
		self.part = base#.cut(cutout)
#		self.part = cutout

	def exportStl(self, path):
#		print("debug")
		self.part.exportStl(path)
