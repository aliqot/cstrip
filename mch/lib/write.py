#!/bin/env python3
import os, sys
from OCC.StlAPI import StlAPI_Writer
from OCC.BRepMesh import BRepMesh_IncrementalMesh

# Modified from https://github.com/tpaviot/pythonocc-core/blob/master/src/Extend/DataExchange.py
def write_stl_file(a_shape, filename, linear_deflection=0.001, angular_deflection=0.5, binary_mode=False):
	""" export the shape to a STL file
	Be careful, the shape first need to be explicitely meshed using BRepMesh_IncrementalMesh
	a_shape: the topods_shape to export
	filename: the filename
	linear_deflection: optional, default to 0.001. Lower, more occurate mesh
	angular_deflection: optional, default to 0.5. Lower, more accurate_mesh
	binary_mode: True - save in ascii format; False - save in binary format
	"""
	if a_shape.IsNull():
		raise AssertionError("Shape is null.")
	if os.path.isfile(filename):
		print("Warning: %s file already exists and will be replaced" % filename)
	# first mesh the shape
	mesh = BRepMesh_IncrementalMesh(a_shape, linear_deflection, False, angular_deflection, True)
	#mesh.SetDeflection(0.05)
	mesh.Perform()
	if not mesh.IsDone():
		raise AssertionError("Mesh is not done.")

	stl_exporter = StlAPI_Writer()
	stl_exporter.SetASCIIMode(not binary_mode)
	stl_exporter.Write(a_shape, filename)

	if not os.path.isfile(filename):
		raise IOError("File not written to disk.")

# script helper functions
def get_output_filename():
	args = sys.argv
	# get rid of script call
	if args.pop(0) in ("python", "python3"):
		args.pop(0)
	# return next argument
	return args.pop(0)
