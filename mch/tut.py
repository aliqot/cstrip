#!/bin/env python3
from OCC.gp import gp_Pnt, gp_Vec, gp_Dir, gp_Ax1, gp_Trsf
from OCC.GC import GC_MakeArcOfCircle as Arc
from OCC.BRepPrimAPI import BRepPrimAPI_MakePrism as Extrude
from OCC.BRepAlgoAPI import BRepAlgoAPI_Fuse as Fuse
from OCC.BRepFilletAPI import BRepFilletAPI_MakeFillet as Fillet
from OCC.BRepBuilderAPI import BRepBuilderAPI_MakeEdge as Edge
from OCC.BRepBuilderAPI import BRepBuilderAPI_MakeWire as Wire
from OCC.BRepBuilderAPI import BRepBuilderAPI_MakeFace as Face
from OCC.BRepBuilderAPI import BRepBuilderAPI_Transform as Transform
from OCC.TopoDS import topods
from OCC.TopExp import TopExp_Explorer
from OCC.TopAbs import TopAbs_EDGE

from lib.write import *
myWidth = 10
myThickness = 5
myHeight = 10
if __name__ == "__main__":
	p1 = gp_Pnt(-myWidth / 2., 0, 0)
	p2 = gp_Pnt(-myWidth / 2., -myThickness / 4., 0)
	p3 = gp_Pnt(0, -myThickness / 2., 0)
	p4 = gp_Pnt(myWidth / 2., -myThickness / 4., 0)
	p5 = gp_Pnt(myWidth / 2., 0, 0)

	arc1 = Arc(p2,p3,p4)
	e1 = Edge(p1, p2).Edge()
	e2 = Edge(arc1.Value()).Edge()
	e3 = Edge(p4, p5).Edge()
	w1 = Wire(e1,e2,e3)

	ax1 = gp_Ax1(gp_Pnt(0,0,0), gp_Dir(1,0,0))
	tfset1 = gp_Trsf()
	tfset1.SetMirror(ax1)
	w2 = topods.Wire(Transform(w1.Wire(), tfset1).Shape())
	combine = Wire()
	combine.Add(w1.Wire())
	combine.Add(w2)

	w3 = combine.Wire()
	f1 = Face(w3).Face()
	v1 = gp_Vec(0,0,myHeight)
	s1 = Extrude(f1, v1)

	s2 = Fillet(s1.Shape())
	anEdgeExplorer = TopExp_Explorer(s1.Shape(), TopAbs_EDGE)

	while anEdgeExplorer.More():
		anEdge = topods.Edge(anEdgeExplorer.Current())
		s2.Add(myThickness / 12.0, anEdge)

		anEdgeExplorer.Next()

	output = s2.Shape()

	write_stl_file(output, get_output_filename(), linear_deflection=.01)

