from lib.Extrusion_Bracket.py import *
## The main part is separate for better modularity (Ex: if i want to use
## this part as a submodule of some other part)
## Note: if __name__ == "__main__" doesn't work in this case
import sys
#	parts = []
#	for i in range(1,10):
#		ea_part = Extrusion_Bracket(thickness = i)
#		ea_part.part.translate(Vector(i*20,0,0))
#		parts.append(ea_part.part)
#	output = Part.Compound(parts)
ea_part = Extrusion_Bracket(thickness = 5)
ea_part.exportStl(sys.argv[2])
