#!/bin/env python3

from lib.write import *
from lib.Extrusion_Connector import *

if __name__ == "__main__":
	output = Extrusion_Connector(thickness = 5).Shape()
	write_stl_file(output, get_output_filename(), linear_deflection=.01)
