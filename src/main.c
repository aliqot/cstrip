//#define F_CPU 20000000UL
#include <avr/io.h>
#include "adc.h"
#include "led.h"
#include "delay.h"
#include "modport.h"

#define thresh 228
#define SCALE 1
int time = 100;
int on = 1;
int val = 0;
char adc[8];
char up = 0;
char down = 0;
int main(void)
{
	MODPORT_init();
	ADC_init();
	DDRA 	|= 0b00001110;
//	PORTA	|= 0b1;
//	DDRA	&=~0b10;

	// Set multiplexer to cap sensors
	DDRB	|= 0b111;
//	PORTB	|= 0b01;
//	PORTB	&=~0b01;
	LED_set_led(0,0,0,6);
//	LED_send_led();

	while(1){
		// ADC read
//		val = (ADC_get_cap(7))/100 -1;
		for (int i = 0; i < 8; i++) {
			MODPORT_data[i] = (ADC_get_cap(i)-thresh) >> SCALE;
			if (MODPORT_data[i] < 0) {
				MODPORT_data[i] = 0;
			}
//			if (MODPORT_extra_data[i]) {
//				LED_set_led(i,MODPORT_extra_data[i],MODPORT_extra_data[i],0);
//			}
//			else {
				LED_set_led(i,MODPORT_data[i],MODPORT_data[i],MODPORT_data[i]);
//			}
		}
		MODPORT_enqueue();
		LED_set_partial(debug0&0x7, 0, 0xf);
		LED_set_partial(debug0>>3, 1, 0xf);
		LED_set_partial(debug1&0x7, 2, 0xf);
//		debug1++;
//		MODPORT_add_to_queue(MODPORT_data[0]);
//		if (MODPORT_is_poppable()) {
//			up = MODPORT_pop();
//		}
//		else {
//			up = 0;
//		}
//		down = MODPORT_contact_minion(MODPORT_data[7]);
//		LED_set_led(0, up, MODPORT_data[0], MODPORT_data[0]);
//		LED_set_led(7, down, MODPORT_data[7], MODPORT_data[7]);
		LED_send_led();
	}
}
