#ifndef MODPORT_H
#define MODPORT_H

void MODPORT_init(void);

char MODPORT_contact_minion(char byte);

int MODPORT_add_to_queue(char byte);

int MODPORT_is_poppable();
int MODPORT_pop();

char MODPORT_flag;

int MODPORT_data[8];
int MODPORT_extra_data[8];
int MODPORT_enqueue();
int debug0; //only for debugging
int debug1; //only for debugging
int debug2; //only for debugging
#endif
