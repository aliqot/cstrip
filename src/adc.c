#include <avr/io.h>
#include "adc.h"

void ADC_init(void) {
	ADMUXA = 0b000000;
	ADCSRA |= 0b10000000;
//	ADCSRB |= 0b1000;
}

int ADC_get_cap(int index) {
	PORTA &= ~0b1110;
	PORTA |= index << 1; // No protection against indices outside 0-7
	PORTB |= 0b110;
	PORTB &=~0b101;
	ADCSRA |= 0b01000000;
	while (ADCSRA & 0b01000000);
	return ADCL | (ADCH << 8);
}
