EESchema Schematic File Version 4
LIBS:19-298 cstrip-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:D D1
U 1 1 5DB38B68
P 5700 900
F 0 "D1" H 5800 850 50  0000 C CNN
F 1 "D" H 5600 850 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5700 900 50  0001 C CNN
F 3 "~" H 5700 900 50  0001 C CNN
	1    5700 900 
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5DB38F2C
P 5700 750
F 0 "R1" V 5700 750 50  0000 C CNN
F 1 "4.7M" V 5600 750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5630 750 50  0001 C CNN
F 3 "~" H 5700 750 50  0001 C CNN
	1    5700 750 
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5DB3B2EB
P 6150 900
F 0 "J1" H 6258 1081 50  0000 C CNN
F 1 "Cap_plate" H 6258 990 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 6150 900 50  0001 C CNN
F 3 "~" H 6150 900 50  0001 C CNN
	1    6150 900 
	-1   0    0    -1  
$EndComp
Text Label 5550 900  2    50   ~ 0
Sq_out
$Comp
L LED:APA102 L5
U 1 1 5DB591F2
P 8500 3000
F 0 "L5" H 8500 3481 50  0000 C CNN
F 1 "APA102" H 8500 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 8550 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 8600 2625 50  0001 L TNN
	1    8500 3000
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 L6
U 1 1 5DB5A725
P 9100 3000
F 0 "L6" H 9100 3481 50  0000 C CNN
F 1 "APA102" H 9100 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 9150 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 9200 2625 50  0001 L TNN
	1    9100 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 2700 8500 2700
Wire Wire Line
	8500 3300 9100 3300
$Comp
L power:VCC #PWR01
U 1 1 5D57E4CD
P 1850 700
F 0 "#PWR01" H 1850 550 50  0001 C CNN
F 1 "VCC" H 1867 873 50  0000 C CNN
F 2 "" H 1850 700 50  0001 C CNN
F 3 "" H 1850 700 50  0001 C CNN
	1    1850 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5D57E9AC
P 1850 1200
F 0 "#PWR03" H 1850 950 50  0001 C CNN
F 1 "GND" H 1855 1027 50  0000 C CNN
F 2 "" H 1850 1200 50  0001 C CNN
F 3 "" H 1850 1200 50  0001 C CNN
	1    1850 1200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 5D57F7C7
P 2750 700
F 0 "#PWR02" H 2750 550 50  0001 C CNN
F 1 "VCC" H 2767 873 50  0000 C CNN
F 2 "" H 2750 700 50  0001 C CNN
F 3 "" H 2750 700 50  0001 C CNN
	1    2750 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5D57FB58
P 2750 1200
F 0 "#PWR04" H 2750 950 50  0001 C CNN
F 1 "GND" H 2755 1027 50  0000 C CNN
F 2 "" H 2750 1200 50  0001 C CNN
F 3 "" H 2750 1200 50  0001 C CNN
	1    2750 1200
	1    0    0    -1  
$EndComp
Text GLabel 1850 900  2    50   Input ~ 0
LOMI_LDR
Text GLabel 1850 800  2    50   Input ~ 0
LIMO_LDR
Text GLabel 1850 1100 2    50   Input ~ 0
CLK_LDR
Text GLabel 2750 1100 0    50   Input ~ 0
CLK_MNN
Text GLabel 2750 900  0    50   Input ~ 0
LOMI_MNN
Text GLabel 2750 800  0    50   Input ~ 0
LIMO_MNN
Text GLabel 1850 1000 2    50   Input ~ 0
MS_LDR
Text GLabel 2750 1000 0    50   Input ~ 0
MS_MNN
Connection ~ 3800 5950
Wire Wire Line
	3700 5950 3800 5950
$Comp
L power:GND #PWR021
U 1 1 5DACB885
P 3800 5950
F 0 "#PWR021" H 3800 5700 50  0001 C CNN
F 1 "GND" H 3950 5900 50  0000 C CNN
F 2 "" H 3800 5950 50  0001 C CNN
F 3 "" H 3800 5950 50  0001 C CNN
	1    3800 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5DAC9C5C
P 4300 5350
F 0 "#PWR019" H 4300 5100 50  0001 C CNN
F 1 "GND" H 4450 5300 50  0000 C CNN
F 2 "" H 4300 5350 50  0001 C CNN
F 3 "" H 4300 5350 50  0001 C CNN
	1    4300 5350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5DAC9107
P 4300 3150
F 0 "#PWR015" H 4300 2900 50  0001 C CNN
F 1 "GND" H 4450 3100 50  0000 C CNN
F 2 "" H 4300 3150 50  0001 C CNN
F 3 "" H 4300 3150 50  0001 C CNN
	1    4300 3150
	1    0    0    -1  
$EndComp
Connection ~ 3800 3750
Wire Wire Line
	3700 3750 3800 3750
$Comp
L power:GND #PWR017
U 1 1 5DAC7951
P 3800 3750
F 0 "#PWR017" H 3800 3500 50  0001 C CNN
F 1 "GND" H 3950 3700 50  0000 C CNN
F 2 "" H 3800 3750 50  0001 C CNN
F 3 "" H 3800 3750 50  0001 C CNN
	1    3800 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR018
U 1 1 5DAC291D
P 3800 4150
F 0 "#PWR018" H 3800 4000 50  0001 C CNN
F 1 "VCC" H 3817 4323 50  0000 C CNN
F 2 "" H 3800 4150 50  0001 C CNN
F 3 "" H 3800 4150 50  0001 C CNN
	1    3800 4150
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR010
U 1 1 5DAC256F
P 3800 1950
F 0 "#PWR010" H 3800 1800 50  0001 C CNN
F 1 "VCC" H 3817 2123 50  0000 C CNN
F 2 "" H 3800 1950 50  0001 C CNN
F 3 "" H 3800 1950 50  0001 C CNN
	1    3800 1950
	1    0    0    -1  
$EndComp
Text GLabel 2150 3250 2    50   Input ~ 0
B
Text GLabel 2150 3150 2    50   Input ~ 0
A
Text GLabel 4300 5650 2    50   Input ~ 0
B
Text GLabel 4300 5550 2    50   Input ~ 0
A
Text GLabel 4300 3450 2    50   Input ~ 0
B
Text GLabel 4300 3350 2    50   Input ~ 0
A
Text GLabel 4300 2450 2    50   Input ~ 0
CAP_IN
Text GLabel 4300 2850 2    50   Input ~ 0
CAP_B
Text GLabel 4300 5050 2    50   Input ~ 0
CAP_C
Text GLabel 4300 4550 2    50   Input ~ 0
LOMI_LED
Text GLabel 4300 4950 2    50   Input ~ 0
CLK_LED
Wire Wire Line
	3100 2550 2150 2550
Wire Wire Line
	3100 4850 3100 2550
Wire Wire Line
	3300 4850 3100 4850
Wire Wire Line
	3200 4450 3300 4450
$Comp
L 4xxx:4052 U3
U 1 1 5DA196AF
P 3800 5050
F 0 "U3" H 4100 5900 50  0000 C CNN
F 1 "4052" H 4050 5800 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3800 5050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4051bms-52bms-53bms.pdf" H 3800 5050 50  0001 C CNN
	1    3800 5050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2150 2250 3300 2250
Text GLabel 5800 2900 0    50   Input ~ 0
LOMI_LED
Text GLabel 5800 3000 0    50   Input ~ 0
CLK_LED
Text GLabel 4300 2650 2    50   Input ~ 0
MS_MNN
Text GLabel 2150 2950 2    50   Input ~ 0
MS_LDR
$Comp
L 4xxx:4052 U2
U 1 1 5D99D649
P 3800 2850
F 0 "U2" H 4100 3700 50  0000 C CNN
F 1 "4052" H 4050 3600 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3800 2850 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4051bms-52bms-53bms.pdf" H 3800 2850 50  0001 C CNN
	1    3800 2850
	-1   0    0    -1  
$EndComp
Text GLabel 4300 4850 2    50   Input ~ 0
CLK_MNN
Text GLabel 4300 2250 2    50   Input ~ 0
LIMO_MNN
Text GLabel 4300 4450 2    50   Input ~ 0
LOMI_MNN
$Comp
L power:GND #PWR016
U 1 1 5D5B8C82
P 1550 3750
F 0 "#PWR016" H 1550 3500 50  0001 C CNN
F 1 "GND" H 1555 3577 50  0000 C CNN
F 2 "" H 1550 3750 50  0001 C CNN
F 3 "" H 1550 3750 50  0001 C CNN
	1    1550 3750
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR09
U 1 1 5D5B8487
P 1550 1950
F 0 "#PWR09" H 1550 1800 50  0001 C CNN
F 1 "VCC" H 1567 2123 50  0000 C CNN
F 2 "" H 1550 1950 50  0001 C CNN
F 3 "" H 1550 1950 50  0001 C CNN
	1    1550 1950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5D5A5402
P 6100 3300
F 0 "#PWR022" H 6100 3050 50  0001 C CNN
F 1 "GND" H 6100 3100 50  0000 C CNN
F 2 "" H 6100 3300 50  0001 C CNN
F 3 "" H 6100 3300 50  0001 C CNN
	1    6100 3300
	1    0    0    -1  
$EndComp
Text GLabel 2150 2650 2    50   Input ~ 0
CLK_LDR
Text GLabel 2150 2850 2    50   Input ~ 0
LOMI_LDR
Text GLabel 2150 2750 2    50   Input ~ 0
LIMO_LDR
$Comp
L power:VCC #PWR020
U 1 1 5D9851A9
P 5650 2700
F 0 "#PWR020" H 5650 2550 50  0001 C CNN
F 1 "VCC" H 5650 2850 50  0000 C CNN
F 2 "" H 5650 2700 50  0001 C CNN
F 3 "" H 5650 2700 50  0001 C CNN
	1    5650 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2700 5650 2700
Wire Wire Line
	3300 2450 3300 2650
Wire Wire Line
	2150 2450 3300 2450
Wire Wire Line
	2150 2350 3200 2350
$Comp
L MCU_Microchip_ATtiny:ATtiny841-SSU U1
U 1 1 5D9C866E
P 1550 2850
F 0 "U1" H 1021 2896 50  0000 R CNN
F 1 "t841" H 1021 2805 50  0000 R CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 1550 2850 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8495-8-bit-AVR-Microcontrollers-ATtiny441-ATtiny841_Datasheet.pdf" H 1550 2850 50  0001 C CNN
	1    1550 2850
	1    0    0    -1  
$EndComp
Connection ~ 8500 3300
Wire Wire Line
	3200 2350 3200 4450
Text GLabel 4300 4650 2    50   Input ~ 0
CAP_A
$Comp
L 4xxx:4051 U4
U 1 1 5DCA137A
P 3800 7350
F 0 "U4" H 4100 8200 50  0000 C CNN
F 1 "4051" H 4050 8100 50  0000 C CNN
F 2 "Package_SO:TSSOP-16_4.4x5mm_P0.65mm" H 3800 7350 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4051bms-52bms-53bms.pdf" H 3800 7350 50  0001 C CNN
	1    3800 7350
	-1   0    0    -1  
$EndComp
Text GLabel 4300 7750 2    50   Input ~ 0
CAP_A
Text GLabel 4300 7850 2    50   Input ~ 0
CAP_B
Text GLabel 4300 7950 2    50   Input ~ 0
CAP_C
$Comp
L power:GND #PWR024
U 1 1 5DCCAB5B
P 4300 7650
F 0 "#PWR024" H 4300 7400 50  0001 C CNN
F 1 "GND" H 4400 7650 50  0000 C CNN
F 2 "" H 4300 7650 50  0001 C CNN
F 3 "" H 4300 7650 50  0001 C CNN
	1    4300 7650
	1    0    0    -1  
$EndComp
Text GLabel 3300 6750 0    50   Input ~ 0
CAP_IN
$Comp
L power:VCC #PWR023
U 1 1 5DCCEB05
P 3800 6450
F 0 "#PWR023" H 3800 6300 50  0001 C CNN
F 1 "VCC" H 3817 6623 50  0000 C CNN
F 2 "" H 3800 6450 50  0001 C CNN
F 3 "" H 3800 6450 50  0001 C CNN
	1    3800 6450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5DCCEFC0
P 3800 8250
F 0 "#PWR025" H 3800 8000 50  0001 C CNN
F 1 "GND" H 3950 8200 50  0000 C CNN
F 2 "" H 3800 8250 50  0001 C CNN
F 3 "" H 3800 8250 50  0001 C CNN
	1    3800 8250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 8250 3800 8250
Connection ~ 3800 8250
Text Label 4300 6750 0    50   ~ 0
Plate_1_analog
$Comp
L LED:APA102 L7
U 1 1 5DD35EC2
P 9700 3000
F 0 "L7" H 9700 3481 50  0000 C CNN
F 1 "APA102" H 9700 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 9750 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 9800 2625 50  0001 L TNN
	1    9700 3000
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 L8
U 1 1 5DD35ECC
P 10300 3000
F 0 "L8" H 10300 3481 50  0000 C CNN
F 1 "APA102" H 10300 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 10350 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 10400 2625 50  0001 L TNN
	1    10300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 2700 9700 2700
Wire Wire Line
	9700 3300 10300 3300
Wire Wire Line
	9100 2700 9700 2700
Connection ~ 9700 2700
Wire Wire Line
	9700 3300 9100 3300
Connection ~ 9700 3300
Connection ~ 9100 2700
Connection ~ 9100 3300
Text Label 4300 6850 0    50   ~ 0
Plate_2_analog
Text Label 4300 6950 0    50   ~ 0
Plate_3_analog
Text Label 4300 7050 0    50   ~ 0
Plate_4_analog
Text Label 4300 7150 0    50   ~ 0
Plate_5_analog
Text Label 4300 7250 0    50   ~ 0
Plate_6_analog
Text Label 4300 7350 0    50   ~ 0
Plate_7_analog
Text Label 4300 7450 0    50   ~ 0
Plate_8_analog
Wire Wire Line
	6100 3300 6700 3300
$Comp
L LED:APA102 L1
U 1 1 5DD877AE
P 6100 3000
F 0 "L1" H 6100 3481 50  0000 C CNN
F 1 "APA102" H 6100 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 6150 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 6200 2625 50  0001 L TNN
	1    6100 3000
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 L2
U 1 1 5DD877B8
P 6700 3000
F 0 "L2" H 6700 3481 50  0000 C CNN
F 1 "APA102" H 6700 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 6750 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 6800 2625 50  0001 L TNN
	1    6700 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2700 6100 2700
$Comp
L LED:APA102 L3
U 1 1 5DD877C3
P 7300 3000
F 0 "L3" H 7300 3481 50  0000 C CNN
F 1 "APA102" H 7300 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 7350 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 7400 2625 50  0001 L TNN
	1    7300 3000
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 L4
U 1 1 5DD877CD
P 7900 3000
F 0 "L4" H 7900 3481 50  0000 C CNN
F 1 "APA102" H 7900 3390 50  0000 C CNN
F 2 "JaxComponentLib:APA102_fixed" H 7950 2700 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 8000 2625 50  0001 L TNN
	1    7900 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2700 7300 2700
Wire Wire Line
	6700 2700 7300 2700
Connection ~ 7300 2700
Connection ~ 6700 2700
Connection ~ 6100 2700
Connection ~ 6100 3300
Connection ~ 6700 3300
Wire Wire Line
	6700 3300 7300 3300
Connection ~ 7300 3300
Wire Wire Line
	7300 3300 7900 3300
Connection ~ 7900 3300
Wire Wire Line
	7900 3300 8500 3300
Wire Wire Line
	7900 2700 8500 2700
Connection ~ 7900 2700
Connection ~ 8500 2700
Wire Wire Line
	5550 900  5550 750 
Connection ~ 5850 900 
Text Label 5850 1150 2    50   ~ 0
Plate_1_analog
Wire Wire Line
	5850 900  5950 900 
Wire Wire Line
	5850 900  5850 1150
Wire Wire Line
	5850 750  5850 900 
$Comp
L Device:D D2
U 1 1 5DC20FF3
P 7000 900
F 0 "D2" H 7100 850 50  0000 C CNN
F 1 "D" H 6900 850 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 7000 900 50  0001 C CNN
F 3 "~" H 7000 900 50  0001 C CNN
	1    7000 900 
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5DC20FF9
P 7000 750
F 0 "R2" V 7000 750 50  0000 C CNN
F 1 "4.7M" V 6900 750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6930 750 50  0001 C CNN
F 3 "~" H 7000 750 50  0001 C CNN
	1    7000 750 
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5DC20FFF
P 7450 900
F 0 "J2" H 7558 1081 50  0000 C CNN
F 1 "Cap_plate" H 7558 990 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 7450 900 50  0001 C CNN
F 3 "~" H 7450 900 50  0001 C CNN
	1    7450 900 
	-1   0    0    -1  
$EndComp
Text Label 6850 900  2    50   ~ 0
Sq_out
Wire Wire Line
	6850 900  6850 750 
Connection ~ 7150 900 
Text Label 7150 1150 2    50   ~ 0
Plate_2_analog
Wire Wire Line
	7150 900  7250 900 
Wire Wire Line
	7150 900  7150 1150
Wire Wire Line
	7150 750  7150 900 
$Comp
L Device:D D3
U 1 1 5DC27C23
P 8300 900
F 0 "D3" H 8400 850 50  0000 C CNN
F 1 "D" H 8200 850 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 8300 900 50  0001 C CNN
F 3 "~" H 8300 900 50  0001 C CNN
	1    8300 900 
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5DC27C29
P 8300 750
F 0 "R3" V 8300 750 50  0000 C CNN
F 1 "4.7M" V 8200 750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8230 750 50  0001 C CNN
F 3 "~" H 8300 750 50  0001 C CNN
	1    8300 750 
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5DC27C2F
P 8750 900
F 0 "J3" H 8858 1081 50  0000 C CNN
F 1 "Cap_plate" H 8858 990 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 8750 900 50  0001 C CNN
F 3 "~" H 8750 900 50  0001 C CNN
	1    8750 900 
	-1   0    0    -1  
$EndComp
Text Label 8150 900  2    50   ~ 0
Sq_out
Wire Wire Line
	8150 900  8150 750 
Connection ~ 8450 900 
Text Label 8450 1150 2    50   ~ 0
Plate_3_analog
Wire Wire Line
	8450 900  8550 900 
Wire Wire Line
	8450 900  8450 1150
Wire Wire Line
	8450 750  8450 900 
$Comp
L Device:D D4
U 1 1 5DC27C5B
P 9600 900
F 0 "D4" H 9700 850 50  0000 C CNN
F 1 "D" H 9500 850 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 9600 900 50  0001 C CNN
F 3 "~" H 9600 900 50  0001 C CNN
	1    9600 900 
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5DC27C61
P 9600 750
F 0 "R4" V 9600 750 50  0000 C CNN
F 1 "4.7M" V 9500 750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9530 750 50  0001 C CNN
F 3 "~" H 9600 750 50  0001 C CNN
	1    9600 750 
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5DC27C67
P 10050 900
F 0 "J4" H 10158 1081 50  0000 C CNN
F 1 "Cap_plate" H 10158 990 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 10050 900 50  0001 C CNN
F 3 "~" H 10050 900 50  0001 C CNN
	1    10050 900 
	-1   0    0    -1  
$EndComp
Text Label 9450 900  2    50   ~ 0
Sq_out
Wire Wire Line
	9450 900  9450 750 
Connection ~ 9750 900 
Text Label 9750 1150 2    50   ~ 0
Plate_4_analog
Wire Wire Line
	9750 900  9850 900 
Wire Wire Line
	9750 900  9750 1150
Wire Wire Line
	9750 750  9750 900 
$Comp
L Device:D D5
U 1 1 5DC3F81B
P 5700 1600
F 0 "D5" H 5800 1550 50  0000 C CNN
F 1 "D" H 5600 1550 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 5700 1600 50  0001 C CNN
F 3 "~" H 5700 1600 50  0001 C CNN
	1    5700 1600
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5DC3F821
P 5700 1450
F 0 "R5" V 5700 1450 50  0000 C CNN
F 1 "4.7M" V 5600 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5630 1450 50  0001 C CNN
F 3 "~" H 5700 1450 50  0001 C CNN
	1    5700 1450
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 5DC3F827
P 6150 1600
F 0 "J5" H 6258 1781 50  0000 C CNN
F 1 "Cap_plate" H 6258 1690 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 6150 1600 50  0001 C CNN
F 3 "~" H 6150 1600 50  0001 C CNN
	1    6150 1600
	-1   0    0    -1  
$EndComp
Text Label 5550 1600 2    50   ~ 0
Sq_out
Wire Wire Line
	5550 1600 5550 1450
Connection ~ 5850 1600
Wire Wire Line
	5850 1600 5950 1600
Wire Wire Line
	5850 1450 5850 1600
$Comp
L Device:D D6
U 1 1 5DC3F853
P 7000 1600
F 0 "D6" H 7100 1550 50  0000 C CNN
F 1 "D" H 6900 1550 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 7000 1600 50  0001 C CNN
F 3 "~" H 7000 1600 50  0001 C CNN
	1    7000 1600
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5DC3F859
P 7000 1450
F 0 "R6" V 7000 1450 50  0000 C CNN
F 1 "4.7M" V 6900 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6930 1450 50  0001 C CNN
F 3 "~" H 7000 1450 50  0001 C CNN
	1    7000 1450
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J6
U 1 1 5DC3F85F
P 7450 1600
F 0 "J6" H 7558 1781 50  0000 C CNN
F 1 "Cap_plate" H 7558 1690 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 7450 1600 50  0001 C CNN
F 3 "~" H 7450 1600 50  0001 C CNN
	1    7450 1600
	-1   0    0    -1  
$EndComp
Text Label 6850 1600 2    50   ~ 0
Sq_out
Wire Wire Line
	6850 1600 6850 1450
Connection ~ 7150 1600
Wire Wire Line
	7150 1600 7250 1600
Wire Wire Line
	7150 1450 7150 1600
$Comp
L Device:D D7
U 1 1 5DC3F88B
P 8300 1600
F 0 "D7" H 8400 1550 50  0000 C CNN
F 1 "D" H 8200 1550 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 8300 1600 50  0001 C CNN
F 3 "~" H 8300 1600 50  0001 C CNN
	1    8300 1600
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5DC3F891
P 8300 1450
F 0 "R7" V 8300 1450 50  0000 C CNN
F 1 "4.7M" V 8200 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8230 1450 50  0001 C CNN
F 3 "~" H 8300 1450 50  0001 C CNN
	1    8300 1450
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J7
U 1 1 5DC3F897
P 8750 1600
F 0 "J7" H 8858 1781 50  0000 C CNN
F 1 "Cap_plate" H 8858 1690 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 8750 1600 50  0001 C CNN
F 3 "~" H 8750 1600 50  0001 C CNN
	1    8750 1600
	-1   0    0    -1  
$EndComp
Text Label 8150 1600 2    50   ~ 0
Sq_out
Wire Wire Line
	8150 1600 8150 1450
Connection ~ 8450 1600
Wire Wire Line
	8450 1600 8550 1600
Wire Wire Line
	8450 1450 8450 1600
$Comp
L Device:D D8
U 1 1 5DC3F8C3
P 9600 1600
F 0 "D8" H 9700 1550 50  0000 C CNN
F 1 "D" H 9500 1550 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-323" H 9600 1600 50  0001 C CNN
F 3 "~" H 9600 1600 50  0001 C CNN
	1    9600 1600
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5DC3F8C9
P 9600 1450
F 0 "R8" V 9600 1450 50  0000 C CNN
F 1 "4.7M" V 9500 1450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9530 1450 50  0001 C CNN
F 3 "~" H 9600 1450 50  0001 C CNN
	1    9600 1450
	0    -1   1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Male J8
U 1 1 5DC3F8CF
P 10050 1600
F 0 "J8" H 10158 1781 50  0000 C CNN
F 1 "Cap_plate" H 10158 1690 50  0000 C CNN
F 2 "JaxComponentLib:capsense" H 10050 1600 50  0001 C CNN
F 3 "~" H 10050 1600 50  0001 C CNN
	1    10050 1600
	-1   0    0    -1  
$EndComp
Text Label 9450 1600 2    50   ~ 0
Sq_out
Wire Wire Line
	9450 1600 9450 1450
Connection ~ 9750 1600
Wire Wire Line
	9750 1600 9850 1600
Wire Wire Line
	9750 1450 9750 1600
NoConn ~ 10600 2900
NoConn ~ 10600 3000
NoConn ~ 4300 5150
NoConn ~ 4300 4750
NoConn ~ 4300 2950
NoConn ~ 4300 2750
NoConn ~ 4300 2550
NoConn ~ 4300 2350
NoConn ~ 2150 3450
Text Label 2150 2250 0    50   ~ 0
LIMO_MUX
Wire Wire Line
	9750 1600 9750 1850
Text Label 9750 1850 2    50   ~ 0
Plate_8_analog
Wire Wire Line
	8450 1600 8450 1850
Text Label 8450 1850 2    50   ~ 0
Plate_7_analog
Wire Wire Line
	7150 1600 7150 1850
Text Label 7150 1850 2    50   ~ 0
Plate_6_analog
Wire Wire Line
	5850 1600 5850 1850
Text Label 5850 1850 2    50   ~ 0
Plate_5_analog
Text Label 2150 3350 0    50   ~ 0
Sq_out
$Comp
L JaxPartsLib:ModPort_Minion P2
U 1 1 5DD76F1F
P 3100 950
F 0 "P2" H 3278 996 50  0000 L CNN
F 1 "ModPort_Minion" H 3278 905 50  0000 L CNN
F 2 "JaxComponentLib:6pad" H 2950 1100 50  0001 C CNN
F 3 "" H 2950 1100 50  0001 C CNN
	1    3100 950 
	1    0    0    -1  
$EndComp
$Comp
L JaxParts:ModPort_Leader P1
U 1 1 5DD773B5
P 1500 950
F 0 "P1" H 1608 1465 50  0000 C CNN
F 1 "ModPort_Leader" H 1608 1374 50  0000 C CNN
F 2 "JaxComponentLib:6pad" H 1350 1100 50  0001 C CNN
F 3 "" H 1350 1100 50  0001 C CNN
	1    1500 950 
	1    0    0    -1  
$EndComp
Text Label 2150 2350 0    50   ~ 0
LOMI_MUX
Text Label 2150 2450 0    50   ~ 0
MS_MUX
Text Label 2150 2550 0    50   ~ 0
CLK_MUX
$EndSCHEMATC
